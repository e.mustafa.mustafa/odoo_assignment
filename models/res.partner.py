from odoo import models, fields

class Contact(models.Model):
    _inherit = 'res.partner'

    contact_type = fields.Selection([
        ('customer', 'Customer'),
        ('lead', 'Lead'),
        ('vendor', 'Vendor'),
        ('employee', 'Employee')
    ], string='Contact Type')