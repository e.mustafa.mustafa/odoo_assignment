from datetime import datetime, time
from odoo import api, models

from dateutil.relativedelta import relativedelta

from odoo import models, fields
from odoo.exceptions import UserError

_INTERVALS = {
    'hours': lambda interval: relativedelta(hours=interval),
    'days': lambda interval: relativedelta(days=interval),
    'weeks': lambda interval: relativedelta(days=7*interval),
    'months': lambda interval: relativedelta(months=interval),
    'now': lambda interval: relativedelta(hours=0),
}

class Event(models.Model):
    _name = 'crm.event'
    _description = 'CRM Event Management'

    name = fields.Char(string='Event Name', required=True)
    date = fields.Datetime(string='Event Date', required=True)
    schedule_date = fields.Datetime(string='Event Date', compute="_compute_schedule_date", store=True)
    interval_nbr = fields.Integer('Interval', default=1)
    interval_unit = fields.Selection([
        ('now', 'Immediately'),
        ('hours', 'Hours'), ('days', 'Days'),
        ('weeks', 'Weeks'), ('months', 'Months')],
        string='Unit', default='hours', required=True)
    participant_ids = fields.Many2many('res.partner', string='Participants', domain=[('customer_rank','>', 0)])
    salesperson_id = fields.Many2one('res.users', string='Salesperson')

    proceed = fields.Boolean("Event Proceed", help="Is this event proceed", readonly=True)

    @api.depends('date', 'interval_nbr', 'interval_unit')
    def _compute_schedule_date(self):
        for event in self:
            event.schedule_date = event.date - _INTERVALS[event.interval_unit](event.interval_nbr)

    @api.model
    def send_event_reminders(self):
        today = datetime.combine(fields.Date.today(), time(0, 0, 0))
        events = self.search([('schedule_date', '<', today), ('proceed', '=', False)])
        for event in events:
            self._send_reminder_email(event)

    def _send_reminder_email(self, event):
        template_id = self.env.ref('CRM_event_management.event_reminder_email_template').id
        template = self.env['mail.template'].browse(template_id)

        for participant in event.participant_ids:
            if participant.user_id:  # Assuming each contact has a responsible salesperson linked as user
                template.with_context(
                    user=participant.user_id.id,
                    event_name=event.name,
                    event_date=event.date,
                    participant_name=participant.name,
                    event_url=f"/web#id={event.id}&view_type=form&model=crm.event"
                ).send_mail(event.id, force_send=True)
        event.write({'proceed': True})

    def action_start_communication(self):
        raise UserError("Sorry! I didn't understand, what is the channel the communication should be ")
        return {
            'type': 'ir.actions.act_window',
            'name': 'Start Communication',
            'res_model': 'crm.messaging',
            'view_mode': 'form',
            'context': {
                'default_event_id': self.id,
                'default_participant_ids': self.participant_ids.ids,
            },
        }