{
    "name": "CRM Event Management",
    "version": "1.0.0",
    'author': 'Mustafa Mustafa',
    'company': 'XYZ ',
    'website': "https://www.odoo.com",
    'summary': """   
        Give sales team an ability to implement a reminder management feature.
    """,
    'description': """ 
      Give sales team an ability to implement a reminder management feature.
    """,
    "sequence": 7,
    "license": "OPL-1",
    "category": "Sales/CRM",
    "depends": ['crm'],
    "data": [
        'views/crm_event_view.xml',
        'data/event_email_template.xml',
        'data/ir_cron_data.xml',
        'security/ir.model.access.csv'
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}