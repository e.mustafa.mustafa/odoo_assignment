from odoo.tests.common import TransactionCase
from datetime import datetime, timedelta
from odoo import fields


class TestEventReminder(TransactionCase):

    def setUp(self):
        super(TestEventReminder, self).setUp()
        self.event_model = self.env['crm.event']
        self.user_model = self.env['res.users']
        self.partner_model = self.env['res.partner']

    def test_event_reminder_email(self):
        # Create a user
        user = self.user_model.create({'name': 'Test User', 'login': 'test_user'})
        # Create a participant with a responsible salesperson linked as user
        participant = self.partner_model.create({'name': 'Test Participant', 'user_id': user.id})
        # Create an event
        event = self.event_model.create({
            'name': 'Test Event',
            'date': fields.Datetime.now(),
            'participant_ids': [(6, 0, [participant.id])],
        })
        # Call the method to send event reminders
        event.send_event_reminders()
        # Check if the mail has been sent
        self.assertTrue(len(event.message_ids) > 0)

    def test_compute_schedule_date(self):
        # Create an event
        event = self.event_model.create({
            'name': 'Test Event',
            'date': fields.Datetime.now(),
            'interval_nbr': 1,
            'interval_unit': 'hours',
        })
        # Compute the schedule date
        event._compute_schedule_date()
        # Check if the schedule date is computed correctly
        self.assertEqual(event.schedule_date, event.date - timedelta(hours=1))

    def test_action_start_communication(self):
        # Create an event
        event = self.event_model.create({
            'name': 'Test Event',
            'date': fields.Datetime.now(),
        })
        # Check if
